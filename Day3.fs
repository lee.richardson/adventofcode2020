﻿module AdventOfCode2020.Day3

open Utils

let isTree (x, y) (map: char list list) =    
    let x = x % map.[0].Length
    List.tryItem y map
    >>= (List.tryItem x)
    |>> (fun c -> c = '#')
    |> Option.defaultValue false

let moveBy (dx, dy) (x, y) = (x + dx, y + dy)

let testSlope slope (input: char list list) =
    List.scan
        (fun state _ -> moveBy slope state)
        (0, 0)
        [0..input.Length] // Hill height
    |> List.map (isTree >< input)
    |> List.filter id
    |> List.length

let part1 input =
    testSlope (3, 1) input
    
let part2 input =
    [ (1, 1); (3, 1); (5, 1); (7, 1); (1, 2) ]
    |> List.map (testSlope >< input)
    |> List.map bigint
    |> List.reduce (*)    

open FParsec.Primitives
open FParsec.CharParsers

let (parser: Parser<_, unit>) =
    (many (many1Till anyChar newline))

go 3 parser part1 part2
