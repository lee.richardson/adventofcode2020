﻿module AdventOfCode2020.Day6

open System
open Utils

open FParsec.Primitives
open FParsec.CharParsers

type Person = char list
type Group = Person list

let uniqueAnswers group =
    group
    |> List.concat
    |> Set.ofList

let sharedAnswers (group: Group) =
    uniqueAnswers group
    |> Set.filter (fun a -> List.forall (List.contains a) group)

let part1 (input: Group list) =
    input
    |> List.map uniqueAnswers
    |> List.map Set.count
    |> List.sum

let part2 input =
    input
    |> List.map sharedAnswers
    |> List.map Set.count
    |> List.sum

let parser = sepBy (many (many1 letter .>> newline) ) newline

go 6 parser part1 part2
