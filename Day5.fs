﻿module AdventOfCode2020.Day5

open System
open Utils

open FParsec.Primitives
open FParsec.CharParsers

let getSeat (x: string) =    
    x.Replace('F', '0').Replace('B', '1')
        .Replace('L', '0').Replace('R', '1')
    |> ((fun x -> (x.Substring(0, 7), x.Substring(7, 3)))
    >> (fun x -> (Convert.ToInt32(fst x, 2), Convert.ToInt32(snd x, 2)))
    >> (fun (row, col) -> (row * 8) + col))

let part1 (input: string list) =
    input
    |> List.filter (String.IsNullOrWhiteSpace >> not)
    |> List.map getSeat
    |> List.max

let part2 input =
    input
    |> List.filter (String.IsNullOrWhiteSpace >> not)
    |> List.map getSeat
    |> List.sort
    |> List.pairwise
    |> List.filter (fun (x, y) -> abs (y - x) <> 1)
    |> List.head
    |> (fun (x, _) -> x + 1)

go 5 (sepBy (restOfLine false) newline) part1 part2
