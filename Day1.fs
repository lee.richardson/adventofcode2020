﻿module AdventOfCode2020.Day1

open Utils
open FParsec.Primitives
open FParsec.CharParsers

module Seq =
    let product = Seq.reduce (*)

let findIt =    
    Seq.find (fun li -> (Seq.sum li) = 2020)
    >> tee (printfn "%A")
    >> Seq.product

let part1 input =
    let input = input |> List.sort
    seq { for i1 in input do
          for i2 in input do yield [i1; i2] }
    |> findIt

let part2 input =
    let input = input |> List.sort
    seq { for i1 in input do
          for i2 in input do
          for i3 in input do yield [i1; i2; i3] }
    |> findIt

go 1 (many (pint32 .>> opt newline)) part1 part2
