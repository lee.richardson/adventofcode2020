﻿module AdventOfCode2020.Day7

open System
open Utils

open FParsec.Primitives
open FParsec.CharParsers

type Container = (string * (int * string) list)
type ContainerMap = Map<string, string list>

let getColoursContained (container: Container) =
    snd container
    |> List.filter (fun (c, _) -> c > 0)
    |> List.map snd

let getParents colour (containers: ContainerMap) =
    Map.filter (fun _ -> List.contains colour) containers
    |> Map.toList
    |> List.map fst

let canContain colour (containers: ContainerMap) =
    let immediateParents = getParents colour containers
    
    let rec collectParents bag =
        let parents = getParents bag containers
        bag :: (List.map collectParents parents |> List.collect id)

    List.fold
        (fun state bag -> state @ collectParents bag)
        []
        immediateParents
    |> List.distinct
    
let part1 (input: Container list) =
    input
    |> List.map (fun c -> (fst c, getColoursContained c))
    |> Map.ofList
    |> canContain "shiny gold"
    |> List.length
    

let part2 (input: Container list) =
    let getBag colour = List.find (fun (c, _) -> c = colour) input
        
    let rec numberOfChildren (container: Container) =
        List.fold
            (fun state (childCount, childColour) ->
                if childCount = 0 then
                    state
                else
                    state + childCount + (childCount * (numberOfChildren (getBag childColour))))
            0
            (snd container)

    numberOfChildren <| getBag "shiny gold"

let colour = many1CharsTill anyChar (skipString " bag" .>> optional (skipChar 's'))
let contained = sepBy1 (pint32 <|> (stringReturn "no" 0) .>> skipChar ' ' .>>. colour) (skipString ", ")

let parser = sepEndBy (colour .>> pstring " contain " .>>. contained .>> skipChar '.') newline

go 7 parser part1 part2
