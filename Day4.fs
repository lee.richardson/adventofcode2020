﻿module AdventOfCode2020.Day4

open System
open Utils

open FParsec.Primitives
open FParsec.CharParsers

type Field = (string * string)
type Passport = Field list

let validate (passport: Passport) =
    let validateField (field: Field) =
        match field with
        | ("byr", v) when fst (Int32.TryParse v) && v.Length = 4 && int v >= 1920 && int v <= 2002 -> true
        | ("iyr", v) when fst (Int32.TryParse v) && v.Length = 4 && int v >= 2010 && int v <= 2020 -> true
        | ("eyr", v) when fst (Int32.TryParse v) && v.Length = 4 && int v >= 2020 && int v <= 2030 -> true
        | ("hgt", v) when v.EndsWith "cm" ->
            let v = v.Split "cm" |> Array.head
            int v >= 150 && int v <= 193
        | ("hgt", v) when v.EndsWith "in" ->
            let v = v.Split "in" |> Array.head
            int v >= 59 && int v <= 76
        | ("hcl", v) when
            v.StartsWith "#"
            && v.Length = 7
            && (Seq.skip 1 v
                |> Seq.forall (fun c -> Char.IsDigit c || List.contains c ['a';'b';'c';'d';'e';'f';])) ->
            true
        | ("ecl", v) when List.contains v ["amb"; "blu"; "brn"; "gry"; "grn"; "hzl"; "oth"] -> true
        | ("pid", v) when v.Length = 9 && fst (Int32.TryParse v) -> true
        | ("cid", _) -> true
        | _ -> false        

    List.forall validateField passport 

let requiredFields = ["byr"; "iyr"; "eyr"; "hgt"; "hcl"; "ecl"; "pid"]

let containsField passport field =
    passport |> List.exists (fun x -> (fst x) = field)

let containsRequiredFields passport =
    requiredFields
    |> List.map (containsField passport)
    |> List.forall id

let part1 (input: Passport list) =
    input
    |> List.filter containsRequiredFields
    |> List.length
    
let part2 input =
    input
    |> List.filter (not << List.isEmpty)
    |> List.map (fun x -> containsRequiredFields x && validate x)
    |> List.filter id
    |> List.length

module Parser =
    let field = many1Chars letter .>> skipChar ':' .>>. many1Chars (noneOf [' ';'\n'])
    let passport = sepEndBy field (skipAnyOf [' '; '\n'])
    
    let (parser: Parser<_, unit>) = sepBy passport newline

go 4 Parser.parser part1 part2
