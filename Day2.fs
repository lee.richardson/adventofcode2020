﻿module AdventOfCode2020.Day2

open Utils
open FParsec.Primitives
open FParsec.CharParsers

type PasswordTest =
    { RuleLower : int
      RuleUpper : int
      Letter    : char
      Password  : string }

let filteredLength f = List.filter f >> List.length

let part1 input =
    let testPassword pw =
        let count = 
            Seq.countBy id pw.Password
            |> Seq.filter (fun (c, _) -> c = pw.Letter)
            |> Seq.tryHead
            |> Option.map snd
            |> Option.defaultValue 0

        count >= pw.RuleLower && count <= pw.RuleUpper

    filteredLength testPassword input
    
let part2 input =
    let testPassword pw =
        (pw.Password.[pw.RuleLower - 1] = pw.Letter)
        <>
        (pw.Password.[pw.RuleUpper - 1] = pw.Letter)
    
    filteredLength testPassword input

let parser: Parser<PasswordTest list, unit> =
    many (
        tuple4
            (pint32)
            (skipChar '-' >>. pint32)
            (pchar ' ' >>. anyChar)
            (skipString ": " >>. restOfLine true)

        |>> fun (ruleLower, ruleUpper, letter, password) ->
            { RuleLower = ruleLower
              RuleUpper = ruleUpper
              Letter = letter
              Password = password }
        )

go 2 parser part1 part2
